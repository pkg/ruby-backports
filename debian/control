Source: ruby-backports
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Jérémy Bobbio <lunar@debian.org>,
           Utkarsh Gupta <utkarsh@debian.org>
Section: ruby
Testsuite: autopkgtest-pkg-ruby
Priority: optional
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               rake,
               ruby-activesupport,
               ruby-test-unit
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-backports
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-backports.git
Homepage: https://github.com/marcandre/backports
XS-Ruby-Versions: all
Rules-requires-root: no

Package: ruby-backports
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: backports of new features for older versions of Ruby
 This Ruby library contains essential backports that enable many of the nice
 features of Ruby 1.8.7 up to 2.0.0 for earlier versions.
 .
 Features are backported with the following limitations:
  - will not break older code,
  - can be implemented in pure Ruby (no C extensions),
  - must pass RubySpec.
